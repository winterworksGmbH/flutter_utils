/// observer that is notified if the app is busy atm (e.g. during network call)

abstract class ObserverAppIsBusy{

  // function that is called to notify the observer of the current state
  void notify(bool isBusy);
}