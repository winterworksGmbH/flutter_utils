import 'package:flutter_utils/listener/observer_app_is_busy.dart';

/// created by Jörg on 15.08.2018
/// subject that notifies its observers whether the app is busy atm
/// since the app can be set to "busy" multiple times asynchronously,
/// we keep track of the [count] so that the app is "unlocked"
/// only if all triggered operations are finished

class SubjectAppIsBusy{

  //----------------------------------------------------------------------------
  // members
  //----------------------------------------------------------------------------
  int count = 0;
  final List<ObserverAppIsBusy> observers = new List();

  //----------------------------------------------------------------------------
  // public usage
  //----------------------------------------------------------------------------
  // add observer
  void subscribe(ObserverAppIsBusy listener){
    observers.add(listener);
  }

  // remove observer
  void unsubscribe(ObserverAppIsBusy listener){
    observers.remove(listener);
  }

  // change busy state
  void changeBusyState(bool busy){
    if (busy){
      count++;
    }
    else {
      count--;
    }

    _notifyObservers();
  }

  //----------------------------------------------------------------------------
  // private usage
  //----------------------------------------------------------------------------
  void _notifyObservers() {
    for (ObserverAppIsBusy o in observers) {
      o.notify(count > 0);
    }
  }
}