// created by Jörg on 14.08.2018
// localization class

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_utils/loca/loca_data.dart';

class AppLocalization {
  //----------------------------------------------------------------------------
  // members
  //----------------------------------------------------------------------------
  final LocaData _locaData = new LocaData();
  final Locale locale;

  //----------------------------------------------------------------------------
  // constructor
  //----------------------------------------------------------------------------
  AppLocalization(this.locale);

  //----------------------------------------------------------------------------
  // factory
  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  //----------------------------------------------------------------------------
  // public usage
  //----------------------------------------------------------------------------
  String getString(String id) {
    return _locaData.getString(locale, id);
  }
}
