// created by Jörg on 14.08.2018
// loca data loaded from file

import 'dart:async';
import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_utils/util/file_helper.dart';

class LocaData {
  //----------------------------------------------------------------------------
  // singleton
  //----------------------------------------------------------------------------
  static LocaData _instance = new LocaData.internal();
  factory LocaData() => _instance;

  LocaData.internal();

  //----------------------------------------------------------------------------
  // members
  //----------------------------------------------------------------------------
  Map<String, dynamic> _localizedValues;
  final FileHelper fileHelper = new FileHelper();

  //----------------------------------------------------------------------------
  // load loca from file
  Future<Null> load() async {
    String data = await fileHelper.read(PathLocation.AssetDir, "loca/loca.txt");
    _localizedValues = json.decode(data);
  }

  //----------------------------------------------------------------------------
  // public usage
  //----------------------------------------------------------------------------
  String getString(Locale locale, String id) {
    // loca available?
    assert(_localizedValues[locale.languageCode].containsKey(id),
        "text with id $id not found!");

    return _localizedValues[locale.languageCode][id];
  }
}
