// created by Jörg on 14.08.2018
// localization delegate

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_utils/loca/app_localization.dart';
import 'package:flutter_utils/util/date_helper.dart';

class AppLocalizationDelegate extends LocalizationsDelegate<AppLocalization> {
  final List<String> supportedLocales;
  const AppLocalizationDelegate(this.supportedLocales);

  @override
  bool isSupported(Locale locale) => supportedLocales.contains(locale.languageCode);

  @override
  Future<AppLocalization> load(Locale locale) {
    new DateHelper().locale = locale;
    return new SynchronousFuture<AppLocalization>(new AppLocalization(locale));
  }

  @override
  bool shouldReload(AppLocalizationDelegate old) => false;
}