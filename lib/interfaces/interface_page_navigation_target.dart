/// interface for the widget that is controlled by [SobrWidgetPageNavigation]

import 'package:flutter/material.dart';

abstract class InterfacePageNavigationTarget{
  // get the tab controller for switching tabs on navigation press
  TabController get tabController;

  // function that is called in the widget if navigation is changed
  void pageChanged(int index);
}