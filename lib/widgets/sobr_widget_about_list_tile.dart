import 'package:flutter/cupertino.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter/material.dart';

class SobrAboutListTile extends StatefulWidget {
  @override
  State createState() => new SobrAboutListTileState();
}

class SobrAboutListTileState extends State<SobrAboutListTile> {

  String version = "Unknown";
  String appName = "SSA";

  @override
  void initState() {
    super.initState();

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      String vAppName = packageInfo.appName;
      String packageName = packageInfo.packageName;
      String pVersion = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;

      if (mounted) {
        setState(() {
          appName = vAppName;
          version = pVersion + " - Build: " + buildNumber;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    return new AboutListTile(
      applicationName: appName,
      applicationVersion: version
    );
  }
}