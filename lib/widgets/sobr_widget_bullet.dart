// bullet widget

import 'package:flutter/cupertino.dart';

class SobrWidgetBullet extends StatelessWidget {
  const SobrWidgetBullet({
    Key key,
    @required this.size,
    @required this.color,
  });

  final double size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(right: size * 2),
      height: size,
      width: size,
      decoration: new BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
    );
  }
}
