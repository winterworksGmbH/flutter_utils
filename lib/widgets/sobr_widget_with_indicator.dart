// created by Jörg on 14.08.2018
// container for a material app that can display a loading indicator
// when a rest call is in progress

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_utils/listener/observer_app_is_busy.dart';
import 'package:flutter_utils/listener/subject_app_is_busy.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SobrWidgetWithIndicator extends StatefulWidget {
  //----------------------------------------------------------------------------
  // members
  //----------------------------------------------------------------------------
  final Widget body;
  final List<SubjectAppIsBusy> subjects;
  final Color indicatorColor;
  final Color backgroundColor;

  //----------------------------------------------------------------------------
  // constructor
  //----------------------------------------------------------------------------
  SobrWidgetWithIndicator({@required this.body, @required this.subjects, @required this.indicatorColor, @required this.backgroundColor});

  //----------------------------------------------------------------------------
  // overides
  //----------------------------------------------------------------------------

  @override
  State<StatefulWidget> createState() {
    return new _SobrWidgetAppWithIndicatorState();
  }
}

class _SobrWidgetAppWithIndicatorState extends State<SobrWidgetWithIndicator>
    implements ObserverAppIsBusy {
  //----------------------------------------------------------------------------
  // members
  //----------------------------------------------------------------------------
  bool isLoading = false;
  bool showIndicator = false;
  Timer timer = null;

  //----------------------------------------------------------------------------
  // overrides
  //----------------------------------------------------------------------------
  @override
  void initState() {
    super.initState();

    // add widget to all observers
    for (SubjectAppIsBusy subject in widget.subjects) {
      subject.subscribe(this);
    }
  }

  @override
  void dispose() {
    for (SubjectAppIsBusy subject in widget.subjects) {
      subject.unsubscribe(this);
    }

    super.dispose();
  }

  @override
  void notify(bool isBusy) {
    // change loading state?
    if (mounted && isBusy != isLoading) {
      setState(() {
        isLoading = isBusy;

        // now loading?
        if (isBusy) {
          showIndicator = true;
        }
      });

      // not loading anymore? then start timer to lazy hide loading screen
      if (!isLoading) {
        if (null != timer) {
          timer.cancel();
        }

        timer = new Timer(Duration(milliseconds: 300), () {
          setState(() {
            showIndicator = false;
          });
        });
      }
    }
  }

  //----------------------------------------------------------------------------
  // build
  @override
  Widget build(BuildContext context) {
    Widget indicator = Container();
    Widget opactiy = Container();

    if (showIndicator) {
      indicator = SpinKitCircle(color: widget.indicatorColor, size: 64.0);
      opactiy = new Opacity(
        opacity: 0.75,
        child: ModalBarrier(dismissible: false, color: widget.backgroundColor),
      );
    }

    //--------------------------------------------------------------------------
    // return content with possible loading indicator
    return new Container(
        child: new Stack(
            alignment: Alignment.center,
            children: <Widget>[widget.body, opactiy, indicator]));
  }
}
