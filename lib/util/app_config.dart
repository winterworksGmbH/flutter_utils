//------------------------------------------------------------------------------
// app config, used for creating different configs for dev, prod etc
//------------------------------------------------------------------------------

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:meta/meta.dart';

// used for prototyping
enum AuditStyle { list, paged_groups, paged_checkpoints }

class AppConfig extends InheritedWidget {
  AppConfig(
      {@required this.appName,
      @required this.baseUrl,
      @required this.auditStyle,
      @required this.themeData,
      @required this.mapsAPIKey,
        this.debugRender,
      @required Widget child}) : super(child: child)
  {
    // paint borders around renderboxes?
    debugPaintSizeEnabled = debugRender ?? false;
  }

  final String appName;
  final String baseUrl;
  final String mapsAPIKey;
  final AuditStyle auditStyle;
  final ThemeData themeData;
  final bool debugRender;

  static AppConfig of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
