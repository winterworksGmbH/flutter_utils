// created by Jörg on 01.08.2018
// helper class for quickly creating default system dialogs

import 'package:flutter/material.dart';

// dialog action ("yes", "no", "cancel")
class SobrDialogAction {
  //--------------------------------------------------------------------------
  // members
  //--------------------------------------------------------------------------
  final String caption;
  final VoidCallback callback;

  //--------------------------------------------------------------------------
  //constructor
  //--------------------------------------------------------------------------
  SobrDialogAction({@required this.caption, this.callback : null});
}

class SobrDialogFactory {
  static AlertDialog create(BuildContext context, String title,
      String text, List<SobrDialogAction> actions) {
    var dialog = new AlertDialog(
        title: new Text(title),
        content: new Text(text),
        actions: List.generate(actions.length, (int index) {
          return new FlatButton(
              child: Text(actions[index].caption),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop(false);
                if (actions[index].callback != null) {
                  actions[index].callback();
                }
              });
        }));

    return dialog;
  }
}
