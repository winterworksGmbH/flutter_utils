// created by Jörg on 28.06.2018
// helper class for date conversions

import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum DateHelperFormat { dMMMMyyyy, ddMMyy, MMMM_yyyy }

class DateHelper {
  //----------------------------------------------------------------------------
  // singleton
  //----------------------------------------------------------------------------
  static DateHelper _instance = new DateHelper.internal();
  factory DateHelper() => _instance;

  DateHelper.internal();

  //----------------------------------------------------------------------------
  // members
  //----------------------------------------------------------------------------
  Locale locale;

  //----------------------------------------------------------------------------
  // public usage
  //----------------------------------------------------------------------------
  bool isSameDay(DateTime dt1, DateTime dt2) {
    return (dt1.year == dt2.year &&
        dt1.month == dt2.month &&
        dt1.day == dt2.day);
  }

  String format(DateTime date,
      [DateHelperFormat format = DateHelperFormat.dMMMMyyyy]) {
    switch (format) {
      case DateHelperFormat.dMMMMyyyy:
        return DateFormat.yMMMMd(locale.toString()).format(date);

      case DateHelperFormat.ddMMyy:
        return DateFormat("dd.MM.yy", locale.toString()).format(date);

      case DateHelperFormat.MMMM_yyyy:
        return DateFormat("MMMM - yyyy", locale.toString()).format(date);

      default:
        return DateFormat.yMMMMd(locale.toString()).format(date);
        break;
    }
  }
}
