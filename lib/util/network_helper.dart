// created by Jörg on 28.06.2018
// general network helper

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class NetworkHelper {
  // singleton
  static NetworkHelper _instance = new NetworkHelper.internal();
  factory NetworkHelper() => _instance;

  NetworkHelper.internal();

  final JsonDecoder _decoder = new JsonDecoder();

  // get
  Future<dynamic> get(String url, Map<String, String> headers) {
/*
    print(' ');
    print('------ REQUEST '+ url + ' ------');
    print('GET ' + url);
    print('Headers ' + headers.toString());
*/
    return http.get(url, headers: headers).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

/*
      print('------ RESPONSE ------');
      print(res);
      print('------ END REQUEST '+ url + ' ------');
      print(' ');
*/
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data, statusCode $statusCode");
      }

      return _decoder.convert(res);
    });
  }

  // post
  Future<dynamic> post(String url, {Map<String, String> headers, body, encoding}) {
    var jsonBody = json.encode(body);

//    print(' ');
//    print('------ REQUEST '+ url + ' ------');
//    print('POST ' + url);
//    print('Headers ' + headers.toString());
//    print(jsonBody);

    return http
        .post(url, body: jsonBody, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while posting data");
      }
//      print('------ RESPONSE ------');
//      print(res);
//      print('------ END REQUEST '+ url + ' ------');
//      print(' ');
      return _decoder.convert(res);
    });
  }

  // post
  Future<int> postStatusCode(String url, {Map<String, String> headers, body, encoding}) {
    var jsonBody = json.encode(body);

//    print(' ');
//    print('------ REQUEST '+ url + ' ------');
//    print('POST ' + url);
//    print('Headers ' + headers.toString());
//    print(jsonBody);

    return http
        .post(url, body: jsonBody, headers: headers, encoding: encoding)
        .then((http.Response response) {

      return response.statusCode;
    });
  }
}

