// created by Jörg on 28.06.2018
// general camera helper

import 'dart:async';
import 'package:camera/camera.dart';

class CameraHelper {
  // singleton
  static CameraHelper _instance = new CameraHelper.internal();
  factory CameraHelper() => _instance;
  CameraHelper.internal();

  //----------------------------------------------------------------------------
  // members
  //----------------------------------------------------------------------------

  List<CameraDescription> _cameras;

  //----------------------------------------------------------------------------
  // functions
  //----------------------------------------------------------------------------

  List<CameraDescription> get cameras => _cameras;

  // init
  Future<Null> init() async {
    _cameras = await availableCameras();
  }
}

