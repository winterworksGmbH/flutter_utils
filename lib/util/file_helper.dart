// created by Jörg on 24.07.2018
// file helper class

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart' show rootBundle;
import 'package:path_provider/path_provider.dart';

enum PathLocation { AppDir, TempDir, AssetDir }

class FileHelper {
  //----------------------------------------------------------------------------
  // singleton
  //----------------------------------------------------------------------------
  static FileHelper _instance = new FileHelper.internal();
  factory FileHelper() => _instance;

  FileHelper.internal();

  //----------------------------------------------------------------------------
  // public usage
  //----------------------------------------------------------------------------

  /// get file with filename for given [PathLocation]
  Future<File> getFile(PathLocation location, String filename) async {
    final path = await getPath(location);
    return File("$path/$filename");
  }

  // read file content
  Future<String> read(PathLocation location, String filename) async {
    // from asset dir?
    if (PathLocation.AssetDir == location) {
      return rootBundle.loadString(filename);
    }

    // or from another path
    File file = await getFile(location, filename);
    return readFromFile(file);
  }

  // read file content
  Future<String> readFromFile(File file) async {
    try {
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If we encounter an error, return empty string
      return "";
    }
  }

  // save file as string
  Future<File> write(
      PathLocation location, String filename, String contents) async {
    File file = await getFile(location, filename);
    return writeToFile(file, contents);
  }

  // save file as string
  Future<File> writeToFile(File file, String contents) async {
    return file.writeAsString(contents, mode: FileMode.writeOnly, flush: true);
  }

  //----------------------------------------------------------------------------
  // load and decode a json file from disc (templates, audits etc)
  Future<dynamic> loadJsonFromDisc(PathLocation path, String relativeFilename) async {
    String data = await read(path, relativeFilename);
    if (data != null && data.isNotEmpty) {
      return json.decode(data);
    }

    return null;
  }

  //----------------------------------------------------------------------------
  // encode and save a json file to disc (templates, audits etc)
  void saveJsonToDisc(PathLocation path, String filename, dynamic content) {
    String value = json.encode(content);
    write(path, filename, value);
  }

  //----------------------------------------------------------------------------
  // delete file
  void deleteFile(PathLocation path, String filename) async {
    File f = await getFile(path, filename);
    f.delete();
  }

  //----------------------------------------------------------------------------
  // private usage
  //----------------------------------------------------------------------------

  /// get path for given [PathLocation]
  Future<String> getPath(PathLocation location) async {
    switch (location) {
      case PathLocation.AssetDir:
        final directory = await getExternalStorageDirectory();
        return directory.path;
        break;

      case PathLocation.AppDir:
        final directory = await getApplicationDocumentsDirectory();
        return directory.path;
        break;

      case PathLocation.TempDir:
        final directory = await getTemporaryDirectory();
        return directory.path;
        break;
    }

    return null;
  }
}
